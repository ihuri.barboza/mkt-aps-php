<?php
    $protocolo = (isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS']=="on") ? "https" : "http");
    $url = '://'.$_SERVER['HTTP_HOST'].'?';
?>
<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <link href="https://cdn.tailwindcss.com" rel="stylesheet">

    <title>
        Educação Adventista
    </title>
    <meta name="description" content="Educação Adventista - Paulista Sul" />
    <meta name="keywords" content="Educação Adventista, Colégio, Escola" />
    <meta name="author" content=" Associação Paulista Sul" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/tailwindcss/2.2.19/tailwind.min.css">
    <!--Replace with your tailwind.css once created-->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,700" rel="stylesheet" />
    <!-- Define your gradient here - use online tools to find a gradient matching your branding-->
    <style>
        .gradient {
            background: linear-gradient(90deg, #2D6BA1 0%, #2D6BA1 100%);
        }
    </style>
</head>

<body class="h-screen overflow-auto flex items-center justify-center" style="background: #FFB200;">
  <!-- Background image -->
  <div class="h-screen bg-cover bg-no-repeat bg-center w-full 
              bg-fixed bg-right-bottom" style="background-image: url(bg-lp.png);">

	<!-- COMPONENT CODE -->


    <div class="max-w-xs rounded overflow-hidden shadow-lg my-2 bg-white
                m-auto">
        <img class="w-full" src="img/enviado.jpeg" alt="Sunset in the mountains">
        <div class="px-6 py-4">
          <div class="font-bold text-xl mb-2 text-blue-900">Recebemos sua mensagem</div>
          <p class="text-grey-darker text-base">
            Que bom você quer conhecer mais sobre a Educação Adventista, logo nossa equipe entrará em contato com você.
          </p>
        </div>
        <div class="px-6 py-4 text-blue-900">
          <span class="inline-block bg-grey-lighter rounded-full px-3 py-1 text-sm font-semibold text-grey-darker mr-2">#EducaçãoAdventista</span>
          <span class="inline-block bg-grey-lighter rounded-full px-3 py-1 text-sm font-semibold text-grey-darker mr-2">#MuitoAlémDoEnsino</span>
          <span class="inline-block bg-grey-lighter rounded-full px-3 py-1 text-sm font-semibold text-grey-darker">#IssoÉIrAlém</span>
        </div>
    </div>

    <script type="text/javascript">
        function redirect() {
            window.location.href = "<?=$protocolo.$url.'source='.$_POST['source'].'&medium='.$_POST['medium'].'&campaign='.$_POST['campaign']?>";
        }

        setTimeout(redirect, 7000);
    </script>

</body>

</html>

<?php
include 'db.php';

$name = $_POST['name'];
$whatsapp = $_POST['whatsapp'];
$message = $_POST['message'];
$school = $_POST['school'];
$source = $_POST['source'];
$medium = $_POST['medium'];
$campaign = $_POST['campaign'];


if (isset ($_POST['name']) && (($_POST['name']) != "") && isset ($_POST['whatsapp']) && (($_POST['whatsapp']) != "") && isset ($_POST['school']) && (($_POST['school']) != "") && isset ($_POST['source']) && (($_POST['source']) != "") && isset ($_POST['medium']) && (($_POST['medium']) != '') && isset ($_POST['campaign']) && (($_POST['campaign']) != '')) {
    
    $sql = 'INSERT INTO leads(name, whatsapp, message, school, source, medium, campaign) VALUES (:name, :whatsapp, :message, :school, :source, :medium, :campaign)';

    $statement = $connection->prepare($sql);
    if ($statement->execute([':name' => $name, ':whatsapp' => $whatsapp, ':message' => $message, ':school' => $school, ':source' => $source,':medium' => $medium, ':campaign' => $campaign])) {
    }

}





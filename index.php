<?php
    if ($_GET['source'] == '' || $_GET['medium'] == '' || $_GET['campaign'] == '') {
        header("Location: erro.php");

        die();
    }
?>
<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <link href="https://cdn.tailwindcss.com" rel="stylesheet">

    <title>
        Educação Adventista
    </title>
    <meta name="description" content="Educação Adventista - Paulista Sul" />
    <meta name="keywords" content="Educação Adventista, Colégio, Escola" />
    <meta name="author" content=" Associação Paulista Sul" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/tailwindcss/2.2.19/tailwind.min.css">
    <!--Replace with your tailwind.css once created-->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,700" rel="stylesheet" />
    <!-- Define your gradient here - use online tools to find a gradient matching your branding-->
    <style>
        .gradient {
            background: linear-gradient(90deg, #2D6BA1 0%, #2D6BA1 100%);
        }
    </style>
</head>

<body class="h-screen overflow-auto flex items-center justify-center" style="background: #FFB200;">
  <!-- Background image -->
  <div class="h-screen bg-cover bg-no-repeat bg-center w-full 
              bg-fixed bg-right-bottom" style="background-image: url(img/bg-lp.png);">

	<!-- COMPONENT CODE -->
	<div class="container mx-auto my-4 px-4 lg:px-20">

		<div class="w-full p-8 my-4 md:px-12 lg:w-9/12 lg:pl-20 lg:pr-40 mr-auto rounded-2xl shadow-2xl bg-white">
			<div class="flex">
				<h1 class="font-bold uppercase text-5xl text-blue-900 opacity-100">Cadastre-se e <br/> entraremos em <br/> contato</h1>
			</div>

			<form action="/insert.php" method="post" id="formID" name="form">

        <div class="grid grid-cols-1 gap-5 my-4">
          <input name="name" class="w-full bg-gray-100 text-gray-900 mt-2 p-3 rounded-lg focus:outline-none focus:shadow-outline
                        focus:ring focus:ring-gray-400 focus:bg-white"
              type="text" placeholder="Nome*" required />
          <input name="whatsapp" class="w-full bg-gray-100 text-gray-900 mt-2 p-3 rounded-lg focus:outline-none focus:shadow-outline
                        focus:ring focus:ring-gray-400 focus:bg-white"
              type="text" placeholder="WhatsApp*" required />
          </div>
          <div class="my-4">
            <textarea name="message" placeholder="Mensagem" class="w-full h-32 bg-gray-100 text-gray-900 mt-2 p-3 rounded-lg focus:outline-none focus:shadow-outline
                        focus:ring focus:ring-gray-400 focus:bg-white"></textarea>
          </div>
  
          <!-- Selecionar unidade escolar -->
          
          <div class="relative inline-flex">
            <svg class="w-2 h-2 absolute top-0 right-0 m-4 pointer-events-none" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 412 232"><path d="M206 171.144L42.678 7.822c-9.763-9.763-25.592-9.763-35.355 0-9.763 9.764-9.763 25.592 0 35.355l181 181c4.88 4.882 11.279 7.323 17.677 7.323s12.796-2.441 17.678-7.322l181-181c9.763-9.764 9.763-25.592 0-35.355-9.763-9.763-25.592-9.763-35.355 0L206 171.144z" fill="#648299" fill-rule="nonzero"/></svg>
            <select name="school" class="block appearance-none w-full bg-gray-100  border-gray-100 px-4 rounded-lg leading-tight focus:bg-white focus:ring focus:ring-gray-400 focus:bg-white
                          text-gray-900 h-10 pl-5 pr-10 bg-white hover:border-gray-400 focus:outline-none appearance-none" required>
              <option>-- Selecione a unidade escolar --</option>
              <option>ALVORADA</option>
              <option>CAMPO DE FORA</option>
              <option>CAMPO LIMPO</option>
              <option>CAPÃO REDONDO</option>
              <option>EMBU DAS ARTES</option>
              <option>ITAPECERICA DA SERRA</option>
              <option>JARDIM LILAH</option>
              <option>JARDIM DAS PALMEIRAS</option>
              <option>PARIQUERA-AÇU</option>
              <option>PIRAJUÇARA</option>
              <option>REGISTRO</option>
              <option>TABOÃO DA SERRA</option>
              <option>VALO VELHO</option>
              <option>VILA DAS BELEZAS</option>
            </select>
          </div>

          <input type="hidden" name="source" value="<?= $_GET["source"] ?  $_GET["source"] : '' ?>">
          <input type="hidden" name="medium" value="<?= $_GET["medium"] ?  $_GET["medium"] : '' ?>">
          <input type="hidden" name="campaign" value="<?= $_GET["campaign"] ?  $_GET["campaign"] : '' ?>">
  
          <!-- Botão para Enviar mensagem-->
          <div class="my-4 w-1/2 lg:w-1/4">
            <button class="uppercase text-sm font-bold tracking-wide bg-blue-900 text-gray-100 p-3 rounded-full w-full 
                        focus:outline-none focus:shadow-outline 
                        hover:bg-blue-600 "
                        type="submit"
                        name="send" id="send" 
            >
              Enviar Mensagem
            </button>
          </div>
        </div>

      </form>

      <!-- Caixa lateral/inferior -->
			<div
				class="w-full lg:-mt-80 lg:w-2/5 px-8 py-12 ml-auto bg-transparent">
				<div class="flex flex-col text-white">
					<img src="img/mat-EA-APS.png" class="max-w-sm object-scale-down h-48 w-96 m-auto" alt="Educação Adventista">

          </div>
          
        </div>
      </div>
    </div>
    <!-- COMPONENT CODE -->
</div>

<!-- CONTATO DIRETO POR WHATSAPP -->
<div class="flex items-end justify-end fixed bottom-0 right-0 mb-4 mr-4 z-10">
  <div>
    <a title="Contato por WhatsApp" href="https://wa.me/551121281010?text=Gostaria%20de%20mais%20informações%20sobre%20a%20Escola%20Adventista" target="_blank" class="block w-16 h-16 rounded-full transition-all shadow hover:shadow-lg transform hover:scale-110 hover:rotate-12">
      <img class="object-cover object-center w-full h-full rounded-full" src="https://img.icons8.com/3d-fluency/48/whatsapp.png"/>
    </a>
  </div>
</div>
</div>
<script type="text/javascript" src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
<script src="custon.js"></script>
</body>
</html>
